import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ThemeModule } from 'theme';

import { BadgesComponent } from './badges';
import { ChipsComponent } from './chips';
import { ComponentsComponent } from './components.component';
import { ProgressBarsComponent } from './progress-bars';
import { SlidersComponent } from './sliders';
import { TogglesComponent } from './toggles';
import { TooltipsComponent } from './tooltips';

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    FormsModule,
  ],
  exports: [
    ComponentsComponent,
  ],
  declarations: [
    ComponentsComponent,
    TogglesComponent,
    ProgressBarsComponent,
    ChipsComponent,
    SlidersComponent,
    TooltipsComponent,
    BadgesComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class ComponentsModule { }
